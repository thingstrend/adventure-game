#include "headers/include.h"
using namespace std;

void NPC::set_health(int h) {
	HP = h;
	//Set, or change, the base health of the character.
}

int NPC::get_health() const {
	return HP;
	//Gives the current HP of the character.
}

int NPC::do_damage() {
	//Apply damage to the character.
	return damage;
}

void NPC::setInventory(Inventory* _items) {
	//Would this be where the XML stuff would come in?
	items = _items;
}

Inventory* NPC::get_inv() const {
	return items;//got to add some stuff here
	//Print the characters inventory
}

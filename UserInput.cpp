
#include "headers/include.h"
using namespace std;

void UserInput::handleTraps(Environment * room, Adventurer *adv){
	Inventory * in = room->getInventory();
	Object** o = in->getItems();
	bool hasItem = false;
	//determine that the trap has not been destroyed
	for(int i = 0; i < in->getItemCount(); i++){
		hasItem = (o[i]->getName() == "Arrow Shooter");
		if(hasItem)
			break;
	}
	Encounter * e = room->getEncounter(2);
	if(e != NULL && hasItem){
		adv->setHealth(adv->getHealth()-e->get_damage());
		cout << "As you entered the " << room->getName() << ", " << e->get_description() << " Your health was decreased to " << adv->getHealth() << "." << endl;
	}
}
//return -1 if item has been used up
int UserInput::handleItemSc(Inventory * in, Adventurer *adv, string cmd, int item){
	Object **items = in->getItems();
	if(in->getItemCount() < item)
		items[item] = NULL;
	Inventory * advInv = adv->getInventory();
	Object **adItems = advInv->getItems();
	if(advInv->getItemCount() < item)
		adItems[item] = NULL;
	if(cmd == "eat" && items[item]->isEdible()){
			cout << "You ate the " << items[item]->getName() << "." << endl;
			Encounter * en = items[item]->encounter(1);
			if(en != NULL){
				if(en->getChange() == "health"){
					adv->setHealth(adv->getHealth()+en->getchangeBy());
				
				cout << en->get_description() << " It was decreased to " << adv->getHealth() << "." << endl;
				}
				/*

				remove item
				*/
				in->removeItem(items[item]);//remove the item
				return -1;//break out of the loop since item is gone
		}
	}
	else if(cmd == "drink" && items[item]->isDrinkable()){
			cout << "You drank the " << items[item]->getName() << "." << endl;
			Encounter * en = items[item]->encounter(5);
			if(en != NULL){
				if(en->getChange() == "health"){
					adv->setHealth(adv->getHealth()+en->getchangeBy());
				
				cout << en->get_description() << " It was increased to " << adv->getHealth() << "." << endl;
				}
				/*

				remove item
				*/
				in->removeItem(items[item]);//remove the item
				return -1;//break out of the loop since item is gone
		}
	}
	else if(cmd =="pickup" && items[item] != NULL){
		if(!items[item]->isMoveable())
			return 0;
		cout << items[item]->getName() << " was added to your inventory" << endl;
		Inventory::transferItem(items[item], in, advInv);
		return -1;
	}
	else if(cmd == "drop" && adItems[item] != NULL){
		if(!adItems[item]->isMoveable())
			return 0;
		cout << adItems[item]->getName() << " was removed from your inventory" << endl;
		Inventory::transferItem(adItems[item], advInv, in);//this does not work here for some reason
		return -1;
	}
	else if(cmd =="describe"){
		cout << items[item]->getDescription() << endl;
	}
	else if(cmd == "take" && items[item] != NULL){
		if(!items[item]->isMoveable())
			return 0;
		//do something
		Encounter * en = items[item]->encounter(6);
		if(en != NULL){
			//must pay to have item
			bool paid = false;
			for(int i = 0; i< in->getItemCount(); i++){
				if(items[i]->getName() == "Silver Coin"){
					paid = true;
					break;
				}
			}
			if(!paid){
				cout << en->get_description() << endl;
			}
			else{
				cout << "You have recieved the drink" << endl;
				Inventory::transferItem(items[item], in, advInv);
				//might segfault here for some reaosn needs more testing
			}
		}
      Encounter * en2 = items[item]->encounter(3);
	      if(en2 != NULL){
	      	bool hasBone = false;
	      	for(int i = 0; i< in->getItemCount(); i++){
					if(items[i]->getName() == "Bone"){
						hasBone = true;
						break;
					}
				}
			if(!hasBone){
				cout << en2->get_description() << endl;
				if(en2->getChange() == "health"){
						adv->setHealth(adv->getHealth()+en2->getchangeBy());
					
					cout << en2->get_description() << " It was decreased to " << adv->getHealth() << "." << endl;	
				}
			}
			else{
				cout << "You got the key" << endl;
				Inventory::transferItem(items[item], in, advInv);
			}
	      }
			return -1;
	}
	else if(cmd == "give" && adItems[item] != NULL){
		if(!adItems[item]->isMoveable())
			return 0;
		//transfer the item to the character
		Inventory::transferItem(adItems[item], advInv, in);
		return -1;
	}
	else if(cmd == "open" && items[item]->getName() == "A Chest"){
		 Encounter * en = items[item]->encounter(7);
		 if(en != NULL){
		 	bool hasKey = false;
		 	for(int i =0; i < advInv->getItemCount(); i++){
		 		if(adItems[i]->getName() == "key"){
		 			hasKey = true;
		 			break;
		 		}
		 	}
		 	if(!hasKey){
		 		cout << en->get_description() << endl;
		 		return -1;
		 	}
		 	else{
		 		cout << "You used the key to unlock the chest." << endl;
		 		cout << "You win! Yeah" << endl;
		 		return -2;
		 	}
		 	
		 }
	}
	else if(cmd == "attack"){
		if(adv->ItemEquipped()){
			Object* equipItem = advInv->getItemByName(adv->getEquip());//get the equiped item
			if(equipItem != NULL){
				items[item]->setDurability(items[item]->getDurability() - equipItem->getDamage());//affect item
				//affect equipped item as well
				equipItem->setDurability(equipItem->getDurability() - equipItem->getDamage() * .01);
				if(items[item]->getDurability() < 1){
					string n = "You have destroyed " + items[item]->getName() + "\n\r";
					in->removeItem(items[item]);
					cout << n;
					return -4;
				}
				else
					cout << "You have weakened the " << items[item]->getName() << endl;
				if(equipItem->getDurability() < 1){
					string n = "your " + equipItem->getName() + " has been destroyed.\n\r";
					advInv->removeItem(equipItem);
					cout << n;
					return -4;
				}
			}
		}
	}
	return 0;
}
string UserInput::outcome(string _act, loadedRooms *rooms, Adventurer *adv) {
	//Each time the user chooses an _action, it will be sent here and checked to see if it can be done.
	//check player health
	if(adv->getHealth() < 1)
	{
		cout << "You have died!. Game over" << endl;
		return "gameOver";
	}
	if(_act == "help"){
		return "Here are a list of helpful commands: \n\rType 1 to find your location \n\rType 2 to see what items are near you.\n\rType 3 to see what items you have in your inventory.\n\rType 4 to examine items in the room.\n\rType 5 to examine items in your inventory.\n\rType 6 to see if there is a character around you.\n\rType 7 to see if the character has any items.\n\rType 8 to attack another opponent.\n\rType north, east, south or west to move \n\rType end to exit the game. \n\r";
	}
	else if(_act == "1"){
		return "You are in a " + rooms->place->getName() + ". " + rooms->place->getDescription()  + "\n\r";
	}
	else if(_act == "2"){
		Inventory * in = rooms->place->getInventory();
		if(in->getItemCount() == 0)
			return "There are no items nearby.\n\r";
		Object **items = in->getItems();
		string returnInven;
		returnInven = "You find the following items: \n\r";
		for(int i = 0; i < in->getItemCount(); i++){
			returnInven = returnInven + "A " + items[i]->getAdjectives() + " " + items[i]->getName() + "\n\r";
		}
	return returnInven;
	}
	else if(_act == "3"){
	Inventory * in = adv->getInventory();
		if(in->getItemCount() == 0)
			return "There are no items in your inventory.\n\r";
		Object **items = in->getItems();
		string returnInven;
		returnInven = "You have the following items: \n\r";
		for(int i = 0; i < in->getItemCount(); i++){
			returnInven = returnInven + "A " + items[i]->getAdjectives() + " " + items[i]->getName() + "\n\r";
		}
		return returnInven;
	}
	else if(_act == "4"){
		Inventory * in = rooms->place->getInventory();
		if(in->getItemCount() == 0)
			return "There are no items nearby.\n\r";
		Object **items = in->getItems();
		cout << "Type the number corrosponding to the item below to examine it \n\r";
		for(int i = 0; i < in->getItemCount(); i++){
			cout << i << ". " << items[i]->getAdjectives() << " " << items[i]->getName() << endl;
		}
		int input;
		cin >> input;
		if(input >= in->getItemCount())
			return "Please try again, invalid command \n\r";
		if( input < in->getItemCount())
			cout << "you chose to examine " << items[input]->getName() << endl;
		while(true){
		cout << "Enter the command below for what you want to do" << endl;
		//check for avaiable things
		cout << "type 'done' when you are finished examining the item." << endl;
		cout << "describe" << endl;
		if(items[input]->isDrinkable())
			cout << "drink" << endl;
		if(items[input]->isEdible())
			cout << "eat" << endl;
		if(items[input]->isMoveable())
			cout << "pickup" << endl;
		if(items[input]->getName() == "A Chest")
			cout << "open" << endl;
		cout << "destroy" << endl;
		cout << "-----------------------" << endl;
		string cmd;
		cin >> cmd;
		if(cmd == "destroy"){
			cout << "What weapon weapon do you want to use?" << endl;
			Inventory * ain = adv->getInventory();
			if(in->getItemCount() == 0)
				return "There are no items in your inventory.\n\r";
			Object **aitems = ain->getItems();
			cout << "Type the number of the item you want to use as your weapon.\n\r";
			for(int i = 0; i < ain->getItemCount(); i++){
				cout << i << ". " << aitems[i]->getAdjectives() << " " << aitems[i]->getName() << endl;
			}
			//equip weapon
			int eput;
			cin >> eput;
			adv->equip(aitems[eput]->getName());
			while(true){
				cout << "What do you want to do?" << endl;
				cout << "attack" << endl;
				cout << "endAttack" << endl;
				cin >> cmd;
				if(cmd == "endAttack")
					return "";
				int status = handleItemSc(in, adv, cmd, input);
				if(status == -4){
					return "";
				}
			}
		}
		if(cmd == "done")
			break;
		int status = handleItemSc(in, adv, cmd, input);
		if(status == -2)
			return "gameOver";
		else if(status == -1)
			break;
		}
		return "skipLine";
	}
	else if(_act == "5"){
		Inventory * in = adv->getInventory();
		if(in->getItemCount() == 0)
			return "There are no items in your inventory.\n\r";
		Object **items = in->getItems();
		cout << "Type the number corrosponding to the item below to examine it \n\r";
		for(int i = 0; i < in->getItemCount(); i++){
			cout << i << ". " << items[i]->getAdjectives() << " " << items[i]->getName() << endl;
		}
		int input;
		int status = 0;
		cin >> input;
		if(input >= in->getItemCount())
			return "Please try again, invalid command\n\r";
		cout << "you chose to examine " << items[input]->getName() << endl;
		while(true){
		cout << "Enter the command below for what you want to do" << endl;
		//check for avaiable things
		cout << "type 'done' when you are finished examining the item." << endl;
		cout << "describe" << endl;
		if(items[input]->isEdible())
			cout << "eat" << endl;
		if(items[input]->isDrinkable())
			cout << "drink" << endl;
		cout << "drop" << endl;
		//define if an item can be given
		NPC * character = rooms->place->getNPC();
		if(character != NULL){
			cout << "give" << endl;
		}
		cout << "-----------------------" << endl;
		string cmd;
		cin >> cmd;
		if(cmd == "done")
			break;
		if(cmd != "give" && cmd != "drop")
			status = handleItemSc(in, adv, cmd, input);
		else if(cmd =="drop"){
			Inventory * room = rooms->place->getInventory();
			status = handleItemSc(room, adv, cmd, input);
		}
		else{
			Inventory * charact = character->get_inv();
			status = handleItemSc(charact, adv, cmd, input);
		}
		if(status == -1)
			break;
		}
		return "skipLine";
	}
	else if(_act == "6"){
		NPC * character = rooms->place->getNPC();
		if(character != NULL){
			return "You find " + character->getName() + ". " + character->getDescription() + "\n\r";
		}
		else
			return "No one is found\n\r";
	}
	else if(_act == "7"){
		NPC * character = rooms->place->getNPC();
		if(character != NULL){
				Inventory * charact = character->get_inv();
			cout << character->getName() + " has the following items:" "\n\r";
			if(charact->getItemCount() == 0){
				return "There are no items in the characters inventory.\n\r";
			}
			Object **items = charact->getItems();
			cout << "Type the number corrosponding to the item below to examine it \n\r";
			for(int i = 0; i < charact->getItemCount(); i++){
				cout << i << ". " << items[i]->getAdjectives() << " " << items[i]->getName() << endl;
			}
			int input;
			cin >> input;
			cout << "you chose to examine " << items[input]->getName() << endl;
			string cmd;
			while(true){
				cout << "Enter one of the following commands. Type done when done." << endl;
				cout << "describe" << endl;
				cout << "take" << endl;
				cin >> cmd;
				if(cmd == "done")
					break;
				int status = handleItemSc(charact, adv, cmd, input);
				if(status == -1)
					break;
			}
			return "skipLine";
		}
		else
			return "No one is found\n\r";
	}
	else if(_act == "8"){
		//This will be the action that starts the attacking phase
	NPC * npc = rooms->place->getNPC();
		if( npc != NULL ) {
			//Attack the character
			//Check your inventory for a weapon to use, if you don't have one, then you can use your fist.
			Inventory * in = adv->getInventory();
			if(in->getItemCount() == 0)
				return "There are no items in your inventory.\n\r";
			Object **items = in->getItems();
			cout << "Type the number of the item you want to use as your weapon.\n\r";
			for(int i = 0; i < in->getItemCount(); i++){
				cout << i << ". " << items[i]->getAdjectives() << " " << items[i]->getName() << endl;
			}
			int input;
			//Maybe have an Equipped option in the XML for each item type?
			cin >> input;
			adv->equip(items[input]->getName());
			cout << "You have seleced the " << items[input]->getName() << endl;
			string cmd;
			while(true){
			cout << "What do you want to do? Enter command:" << endl;
			cout << "attack" << endl;
			cout << "endAttack" << endl;
			cin >> cmd;
			if(cmd == "endAttack")
				break;
			if(cmd == "attack"){
			//if adventurer has a weapon equipped.
			if(adv->ItemEquipped()){
				//Function to get damage of an item
				NPC * opponent = rooms->place->getNPC();
					  srand (time(NULL));

				/* generate secret number between 1 and 100: */
					int achance = rand() % 50 + 1;
					if(achance < 26){
					opponent->set_health(opponent->get_health()-items[input]->getDamage());
					cout << "You attacked " << opponent->getName() << "." << endl;
					cout << "Opponents health is now " << opponent->get_health() << endl;
					items[input]->setDurability(items[input]->getDurability() - ( opponent->get_health() * .01));
					if(opponent->get_health() < 1)
							{
								cout << "You have killed "<< opponent->getName()  << ". Your opponent has dropped the following items" << endl;
								Inventory * oin = opponent->get_inv();
								Object **Oinventory = oin->getItems();
								//have character drop all items
								for(int f = 0; f < oin->getItemCount(); f++){
									cout << Oinventory[f]->getName() << endl;
									Inventory::transferItem(Oinventory[f], oin, rooms->place->getInventory());;
								}
								rooms->place->setNPC(NULL);
								opponent = NULL;
								return "";//break out battle is over
							}
				}
				else{
					cout << opponent->getName() << " dodged your attack" << endl;
				}
				//handle item destruction
				if(items[input]->getDurability() < 1){
					string n = items[input]->getName() + " got destroyed.\n\r";
					adv->equip("");
					in->removeItem(items[input]);
					return n;
				}
					//Decrease durability too
					//the opponent might choose to attack
						//try to dodge out of the way
			//Random chance of actually dodging the attack.
			  /* initialize random seed: */
				  srand (time(NULL));

				/* generate secret number between 1 and 100: */
				int chance = rand() % 100 + 1;
					if( chance >= 50 )
						cout << "You dodged " << opponent->getName() << "'s counter attack!" << endl;
					else {
							adv->setHealth(adv->getHealth()-opponent->do_damage());
							cout << "You were attacked by " << opponent->getName() << "." << " Your health is now " << adv->getHealth() << endl;
						if(adv->getHealth() < 1)
							{
								cout << "You have died!. Game over" << endl;
								return "gameOver";
							}
						}
				}
				
		}
			}		
		}
	}
	//need to check if there is something there before we can move there
	else if(_act == "north"){
		//They move forward.
		if(rooms->place->getUp() != NULL){
			rooms->place = rooms->place->getUp();
			handleTraps(rooms->place, adv);
			return "You are in a " + rooms->place->getName() + ". " + rooms->place->getDescription()  + "\n\r";
		}
		else
			return rooms->place->getDeadEnd()+"\n\r";
	}
	else if(_act == "west") {
		//move left
		if(rooms->place->getLeft() != NULL){
			rooms->place = rooms->place->getLeft();
			handleTraps(rooms->place, adv);
			return "You are in a " + rooms->place->getName() + ". " + rooms->place->getDescription()  + "\n\r";
		}
		else
			return rooms->place->getDeadEnd()+"\n\r";
	}
	else if(_act == "east") {
		//move right
		if(rooms->place->getRight() != NULL){
			rooms->place = rooms->place->getRight();
			handleTraps(rooms->place, adv);
			return "You are in a " + rooms->place->getName() + ". " + rooms->place->getDescription()  + "\n\r";
		}
		else
			return rooms->place->getDeadEnd()+"\n\r";
	}
	else if(_act == "south" /*Or back*/) {
		//move back
	if(rooms->place->getDown() != NULL){
		rooms->place = rooms->place->getDown();
		handleTraps(rooms->place, adv);
		return "You are in a " + rooms->place->getName() + ". " + rooms->place->getDescription()  + "\n\r";
	}
	else
		return rooms->place->getDeadEnd()+"\n\r";
	//something like rooms->place-> = rooms->place->getDown();
	}
	return "Unreconizable commands\n\r" ;
	//etc. etc. etc.
	//A List of actions will be needed to fill this out, but for now we really only need the basics.
}

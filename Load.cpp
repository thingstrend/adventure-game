#include "headers/include.h"
/*
for draft need 5 rooms and 3 items
*/
using namespace pugi;
//reference http://pugixml.googlecode.com/svn/tags/latest/docs/manual/access.html#manual.access.text
/*
static **Encounter loadObjects(xml_node items)
@parem takes a xml node
@returns list of encoutners
@desc parses in the following format
<trap>
  <type></type>
  <damage></damage>
	<description></description>
	<triggerType></triggerType>
</trap>
<scenario>
  <description></description>
  <triggerType></triggerType>
  <outcome></outcome>
</scenario>
*/
Encounter** Load::loadEncounters(xml_node items, int &i){
  i = 0;
   if(strcmp(items.text().get(), "NULL") == 0)
    return NULL;
  xml_node traps = items.child("traps");
  xml_node scenarios = items.child("scenarios");
  Encounter **list;
	for(xml_node item = items.first_child(); item; item = item.next_sibling()){
		i++;
	}
  int count = 0;
	//this needs to be reworked to include both the trap and encounter
	list = new Encounter*[i];
	for(xml_node trap = traps.first_child(); trap; trap = trap.next_sibling()){
    
		string description = trap.child("description").text().get();
		int triggerType = trap.child("triggerType").text().as_int();
    string type = trap.child("type").text().get();
    int damage = trap.child("damage").text().as_int();
    Trap *tr = new Trap(type, damage);
    tr->set_description(description);
    tr->setTriggerType(triggerType);
		list[count] = tr;
		count++;
	}
  for(xml_node scenario = scenarios.first_child(); scenario; scenario = scenario.next_sibling()){
    
    string description = scenario.child("description").text().get();
    string preferred_act = scenario.child("preferred_act").text().get();
    string change = scenario.child("change").text().get();
    int changeby = scenario.child("changeBy").text().as_int();
    int triggerType = scenario.child("triggerType").text().as_int();

    Scenario *sc = new Scenario(preferred_act);
    sc->set_description(description);
    sc->setTriggerType(triggerType);
    sc->setChange(change);
    sc->setchangeBy(changeby);
    list[count] = sc;
    //more stuff
    count++;
  }
	return list;
}

/*
static **Object Load::loadObjects(xml_node items)
@parem takes an xml node
@returns list of objects
@desc parses in the follow format
<object>
	<name></name>
	<description></description>
	<adjectives></adjectives>
	<size></size>
	<weight></weight>
	<durability></durability>
	<damage></damage>
	<luminosity></luminosity>
	<edible></edible>
    <drinkable></drinkable>
	<moveable></moveable>
	<encounters>
		<encounter>
			<description></description>
			<triggerType></triggerType>
		</encounter>
	</encounters>
</object>
*/
Object** Load::loadObjects(xml_node inventory, int &i){


  i = 0;
  if(strcmp(inventory.text().get(), "NULL") == 0)
    return NULL;

 Object **list;
 	//loop through once and get a count
 for(xml_node items = inventory.first_child(); items; items = items.next_sibling()){
   	i++;
   }
   list = new Object*[i];
   int count = 0;
   for(xml_node obj = inventory.first_child(); obj; obj = obj.next_sibling()){
   		string name = obj.child("name").text().get();
   		string description = obj.child("description").text().get();
   		string adjectives = obj.child("adjectives").text().get();
   		//Not sure how to do this right
   		 Object *o = new Object(name, description, adjectives);
   		//setters
   		int size = obj.child("size").text().as_int();
   		int weight = obj.child("weight").text().as_int();
   		int durability = obj.child("durability").text().as_int();
   		int damage = obj.child("damage").text().as_int();
   		int luminosity = obj.child("luminosity").text().as_int();
   		bool flammable = obj.child("flammable").text().as_bool();
   		bool edible = obj.child("edible").text().as_bool();
      bool drinkable = obj.child("drinkable").text().as_bool();
   		bool moveable = obj.child("moveable").text().as_bool();
   		//get any encoutners
      int eSize = 0;
   		Encounter **Elist = loadEncounters(obj.child("encounters"), eSize);
   		o->setSize(size);
   		o->setWeight(weight);
   		o->setDurability(durability);
   		o->setDamage(damage);
   		o->setLuminosity(luminosity);
   		o->setFlammable(flammable);
   		o->setEdible(edible);
      o->setDrinkable(drinkable);
   		o->setMoveable(moveable);
   		o->setEncounters(Elist, eSize);//need to get the count not sure how
   		list[count] = o;
   		count++;
   }
   return list;
}

/*
*Environment Load::findPlace(int id)
@desc - helps with connecting rooms togethor
@param - takes a room id
@returns pointer to room
*/
Environment* Load::findPlace(int id){
loadedRooms *walker = first;
  if(walker != NULL){
    while(walker->next != NULL && walker->place->getId() != id){
      walker = walker->next;
    }
    if(walker->place->getId() == id){
      return walker->place;
    }
    else
      return NULL;
  }
  else
    return NULL;
}


NPC* Load::loadNPC(xml_node item){
  if(strcmp(item.text().get(), "NULL") == 0)
    return NULL;
  int health = item.child("health").text().as_int();
  int damage = item.child("damage").text().as_int();
  string name = item.child("name").text().get();
  string description = item.child("description").text().get();
  NPC *c = new NPC(health, damage, name, description);
  //
  int iSize = 0;
  Object **list = loadObjects(item.child("inventory"), iSize);
  Inventory *sack = new Inventory(list, iSize);//need to get size of inventory not sure how
  c->setInventory(sack);
  return c;
}
/*
Load::Load(string filename)
@parem - takes xml file
@desc - parses xml to game
<environments>
  <environment>
    <name></name>
    <description></description>
    <id></id>
    <left></left>
    <right></right>
    <up></up>
    <down></down>
    <inventory>
      <object>
      </object>
    </inventory>
    <characterr>
        <health></health>
        <damage></damage>
        <name></name>
        <econunters>
         <encounter>
          <encounter>
        </encounters>
    </character>
    <econunters>
      <encounter>
      <encounter>
    </encounters>
  </environment>
</environments>
*/

Load::Load(){
  xml_document doc;
	xml_document doc2;
    if (doc.load_file("adv.xml") && doc2.load_file("env.xml")){
      first = NULL;
    	//load up adventurer
    	  xml_node adv = doc.child("adventurer");
    	  int health = adv.child("health").text().as_int();
    	   string name = adv.child("name").text().get();
    	  xml_node inventory = adv.child("inventory");
    	  //loop through and get the inventory objects for adventurer
        int iSize = 0;
    	  Object **list = loadObjects(inventory, iSize);
    	  Inventory *sack = new Inventory(list, iSize);//need to get size of inventory not sure how
    	  loadedPerson = new Adventurer(sack, health, name);
    	  //load up environment
    	  xml_node environments = doc2.child("environments");
        int i = 0;
        for(xml_node environment = environments.first_child(); environment; environment = environment.next_sibling()){
          int id = environment.child("id").text().as_int();
    	  	string e_name = environment.child("name").text().get();
    	  	string e_description = environment.child("description").text().get();
          int readLeft = environment.child("left").text().as_int();
          int readRight = environment.child("right").text().as_int();
          int readUp = environment.child("up").text().as_int();
          int readDown = environment.child("down").text().as_int();
          string e_end = environment.child("deadEnd").text().get();
          //inventory
          int eiSize = 0;
           xml_node nodeEnInventory = environment.child("inventory");
          Object **iList = loadObjects(nodeEnInventory, eiSize);
          Inventory *en_inven = new Inventory(iList, eiSize);
          Environment* left;
          Environment* right;
          Environment* up;
          Environment* down;
          if(readLeft == -1)
            left = NULL;
          else{
            left = findPlace(readLeft);
          }
          if(readRight == -1)
            right = NULL;
          else{
            right = findPlace(readRight);
          }
          if(readUp == -1)
            up = NULL;
          else{
            up = findPlace(readUp);
          }
          if(readDown == -1)
            down = NULL;
          else{
            down = findPlace(readDown);
          }
    	  	//attatch this to room somehow
          NPC * charact = loadNPC(environment.child("character"));
          //check environment directions

          //load in the environment
          Environment * e = new Environment(e_name, e_description, id, en_inven);
          e->setDeadEnd(e_end);
          e->setNPC(charact);
          //encounters
          int eSize = 0;
          Encounter **Elist = loadEncounters(environment.child("encounters"), eSize);
          //need to hook this up with environment
          e->setEncounter(Elist, eSize);
          //set the direction reverse way
          if(left != NULL)
            left->setRight(e);
          if(right != NULL)
            right->setLeft(e);
          if(up != NULL)
            up->setDown(e);
          if(down != NULL)
            down->setUp(e);
          //set diretion of current room
          e->setLeft(left);
          e->setRight(right);
          e->setUp(up);
          e->setDown(down);
          //need to 
          loadedRooms *placeWalker; 
          if(i == 0){//first one
            loadedRooms *store = new loadedRooms;
            store->place = e;
            store->next = NULL;
            placeWalker = store;
            first = placeWalker;
          }
          else{
            loadedRooms *store = new loadedRooms;
            store->place = e;
            store->next = NULL;
            placeWalker->next = store;
            placeWalker = placeWalker->next;
          }
          i++;
    	  }
    }
}
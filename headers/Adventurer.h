using namespace std;
class Adventurer{
	private:
		Inventory *inventory;
		int health;
		string name;
		string weapon;
	public:
		Adventurer(Inventory* _inventory, int _health, string _name):health(_health),name(_name){
			inventory = _inventory;
			weapon = "";
		};
		void setHealth(int h){
			health = h;
		}
		int getHealth() const{
			return health;
		}
		string getName() const{
			return name;
		}
		Inventory* getInventory() const{
			return inventory;
		}
		//In Adventurer
//Add a function for equipping an item

	void equip(string name){
		weapon = name;
	}
	bool ItemEquipped() {
		return weapon != "";
	}

	string getEquip() {
		return weapon;
	}
};
//using namespace std;
#include <string>
//This class handles the user input and determines what is going on depending on the scenario and the location.

class UserInput {
	public:
		string outcome(string act,loadedRooms *rooms, Adventurer *adv);		//Determine the outcome.
		int handleItemSc(Inventory * in, Adventurer *adv, string cmd, int item);
		void handleTraps(Environment * room, Adventurer *adv);
		//Outcome would consist of many if statements that would determine what would happen depending on the action.
};

using namespace std;

//Nonplayer character, this is to be used for animals or other people in the world that you cannot play as

class NPC {
	private:
		int HP;	//The amount of health that the character has
		int damage; //The amount of damage that the character can do, presumably unarmed.
		string name; //The name of the character.
		string description;
		Inventory *items;		//The items that the NPC is carrying.
	public:
		NPC(int h, int d, string n, string des) : HP(h), damage(d), name(n), description(des){};	//Initialize the basics
		~NPC();
		void setInventory(Inventory *_items);
		string getName() const{ return name;};
		string getDescription() const{ return description;};
		void set_health(int h);	//Set the base HP of the character
		int get_health() const;		//Return the characters health
		int do_damage();		//Use this to deal damage to the character
		Inventory* get_inv() const;		//Set up the Inventory for the character
};

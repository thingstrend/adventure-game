using namespace std;
using namespace pugi;
struct loadedRooms{
	loadedRooms *next;
	Environment *place;
};
class Load{
	private:
		loadedRooms *first;
		Adventurer *loadedPerson;
		Encounter** loadEncounters(xml_node items, int &i);
		Object** loadObjects(xml_node items, int &i);
		NPC* loadNPC(xml_node item);
		Environment* findPlace(int id);
	public:
		//constructor loads contents of file into memory
		Load();
		loadedRooms* getEnvironment(){
			return first;
		};
		Adventurer * getAdventurer(){
			return loadedPerson;
		}
		//deconstructor
		~Load();
};


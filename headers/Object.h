using namespace std;
//will be removed later just for compile sake
/*
class Encounter{
public:
int checkTrigger();
};*/
class Object{
	private:
		int size, weight, durability, damage, luminosity, encounters; 
		string name, description, adjectives;//descriptions of object
		Encounter **scenarios;
		bool consumed, flammable, edible, moveable, drinkable;
	public:
		//constructor
		Object(string _name, string _description, string _adjective):name(_name),description(_description),adjectives(_adjective){
			encounters = 0;
		};
		//deconstructor
		~Object(){
			//uncomment when encounter is done
			/*
			for(int i = 0; i < encounters; i++){
				delete scenarios[i];
			}
			if(encounters > 0)
			delete [] scenarios;*/
		}
		//copy construcotr
		Object(const Object *rhs){
			size=rhs->size;
			weight=rhs->weight;
			durability=rhs->durability;
			damage=rhs->damage;
			luminosity=rhs->luminosity;
			encounters=rhs->encounters;
			name=rhs->name;
			description=rhs->description;
			adjectives=rhs->adjectives;
			scenarios=rhs->scenarios;
			flammable=rhs->flammable;
			edible=rhs->edible;
			consumed = rhs->consumed;
			moveable=rhs->moveable;
			drinkable = rhs->drinkable;
		}
		//getters and setters
		void setSize(int _size){
			size = _size;
		}
		int getSize() const{
			return size;
		}
		void setWeight(int _weight){
			weight = _weight;
		}
		int getWeight() const{
			return weight;
		}
		void setDurability(int _durability){
			durability = _durability;
		}
		int getDurability() const{
			return durability;
		}
		void setDamage(int _damage){
			damage = _damage;
		}
		int getDamage() const{
			return damage;
		}
		void setLuminosity(int _luminosity){
			luminosity = _luminosity;
		}
		int getLuminosity() const{
			return luminosity;
		}
		string getName() const{
			return name;
		}
		string getDescription() const{
			return description;
		}
		string getAdjectives() const{
			return adjectives;
		}
		void setConsumed(bool val){
			consumed = val;
		}
		bool isConsumed() const{
			return consumed;
		}
		void setFlammable(bool val){
			flammable = val;
		}
		bool isFlammable() const{
			return flammable;
		}
		void setEdible(bool val){
			edible = val;
		}
		bool isEdible() const{
			return edible;
		}
		void setDrinkable(bool val){
			drinkable = val;
		}
		bool isDrinkable() const{
			return drinkable;
		}
		void setMoveable(bool val){
			moveable = val;
		}
		bool isMoveable() const{
			return moveable;
		}
		void setEncounters(Encounter * list[], int _size){
			encounters = _size;
			scenarios = list;
		}
		//actions
		bool isBroken() const{
			return durability == 0;
		}
		//check for object encounter types
		Encounter* encounter(int type) const{
			
			for(int i = 0; i < encounters; i++){
				if(scenarios[i]->checkTrigger() == type){
					return scenarios[i];
				}
			}
			return  NULL;
		}


};



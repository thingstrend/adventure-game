#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <iostream>
#define PUGIXML_HEADER_ONLY
#include "xml/pugixml.hpp"//xml library see http://pugixml.googlecode.com/svn/tags/latest/docs/quickstart.html
#include "Encounter.h"
#include "Object.h"
#include "Inventory.h"
#include "Adventurer.h"
#include "NPC.h"
#include "Environment.h"
#include "Load.h"
#include "UserInput.h"
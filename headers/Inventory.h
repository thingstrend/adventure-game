using namespace std;
class Inventory{
private:
	Object **items;
	int count;
	bool contains(Object * item);

public:
	Inventory(Object * list[], int num){
		count = num;
		items = list;
	};
	void addItem(Object * o);
	int getItemCount() const{
		return count;
	}
	Object * getItemByName(string name) const;
	Object ** getItems() const{
		return  items;
	}
	void removeItem(Object * item);
	static void transferItem(Object* obj,Inventory* inv1, Inventory* inv2);
};
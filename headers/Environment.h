using namespace std;

//need to implement Encounters here as well as characters
class Environment {
	private:
	Environment* _left;
	Environment* _right;
	Environment* _up;
	Environment* _down;
	Environment* _next; // holds the nodes before we initialize left,right,down
	Inventory* inventory;
	Encounter** _encounter; //
	NPC* _npc;
	string name;
	string description;
	int id;
	string deadEnd;
	int encounters;
public:
	// initially sets rooms next to it as null
	Environment(std::string _name, std::string _description, int _id, Inventory * _inventory):name(_name),description(_description),id(_id){
		//copy in items
		inventory = _inventory;
		encounters = 0;
	}
	~Environment();

	// text getter and setters
	int getId() const{
		return id;
	}
	string getName() const{
		return name;
	}
	void setName(std::string _name){
		name = _name;
	}
	string getDescription() const{
		return description;
	}
	void setDescription(string _description)
	{
		description = _description;
	}
	void setDeadEnd(string end){
		deadEnd = end;
	}
	string getDeadEnd() const{
		return deadEnd;
	}
	void	addItem(Object* item);
	void	removeItem(std::string name);
	void	setLeft(Environment* direction){
		_left = direction;
	};
	void	setRight(Environment* direction){
		_right = direction;
	};
	void	setUp(Environment* direction){
		_up = direction;
	}
	void	setDown(Environment* direction){
		_down = direction;
	}
	Inventory* getInventory() const{
		return inventory;
	}
	Environment*	getLeft() const{
		return _left;
	};
	Environment*	getRight() const{
		return _right;
	};	
	Environment*	getUp() const{
		return _up;
	}
	Environment*	getDown() const{
		return _down;
	}
	void	setNPC(NPC* npc){
		_npc = npc;
	}
	void 	setEncounter(Encounter* e[],int esize){
		_encounter = e;
		encounters = esize;
	}
	NPC* getNPC() const{
		return _npc;
	}
	Encounter** getEncounters() const{
		return _encounter;
	}
	Encounter* getEncounter(int type) const{
		for(int i = 0; i < encounters; i++){
				if(_encounter[i]->checkTrigger() == type){
					return _encounter[i];
				}
			}
			return  NULL;
	}
};


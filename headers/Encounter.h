//Encounters entail those that are physical, such as traps, and those that are situational, such as scenarios
using namespace std;
class Encounter {
	private:
		int triggerType;//trigger
		int changeBy;
		string change;
	protected:
		string description; //A description of the event or the trap itself.
		//Will set the description
	public:
		//set trigger type
		void setTriggerType(int type){
			triggerType = type;
		};
		int checkTrigger() const{
			return triggerType;
		};//get the type of trigger
		void setchangeBy(int c){
			changeBy = c;
		}
		int getchangeBy() const{
			return changeBy;
		}
		void setChange(string cmd){
			change = cmd;
		}
		string getChange() const{
			return change;
		}
		virtual void set_description(string desc) = 0;
		virtual void set_act(string act){};
		virtual string get_description() const = 0;
		virtual void set_damage(int d){};	//Sets the amount of damage that the trap can do. Set it to the players HP for a fatal trap.
		virtual int get_damage() const{return -1;};
		virtual void set_pa(string p){};		//Set the preferred act.
		virtual string get_pa() const {return "";};
		virtual string final_outcome(string act) const{return "";};	
		virtual void set_type(string t){};	//Set the type of trap
		virtual string get_type(){ return "";};	//Set the type of trap
};


//These are purely physical traps that cause damage to the player. They can be triggered with certain actions.

//These are purely physical traps that cause damage to the player. They can be triggered with certain actions.
class Trap : public Encounter {
	private:
		string type;		//The type of trap that you are encountering. Can be: trip wire, button, pressure plate.
		int damage;			//The amount of damage that the trap will do. Can be fatal.
	public:
		Trap(string t, int d) : type(t), damage(d) {};
		~Trap();
		void set_damage(int d);	//Sets the amount of damage that the trap can do. Set it to the players HP for a fatal trap.
		int get_damage() const;	//Tells how much damage it would do. Preferably through short phrases like, "That looks like it would hurt a lot" instead of "It would do 20 damage."
		void set_description(string desc);	//Set up the description of the trap.
		string get_description() const;		//Prints the description.
		void set_type(string t);	//Set the type of trap
		virtual string get_type(){return type;};	//Set the type of trap
};


//Scenarios are situational in nature and can be potentially fatal depending on your reaction.
class Scenario : public Encounter {
	private:
		string preferred_act;	//The preferred act of the player.
		string action;		//What the character chooses to do.
		friend class UserInput;		//Gives it access to the actual input.
	public:
		Scenario(string pa) : preferred_act(pa) {};
		~Scenario();
		void set_act(string act);
		void set_pa(string p);		//Set the preferred act.
		string get_pa() const;
		void set_description(string d);		//Set the description of the event/scenario
		string get_description() const;		//Get the description
		string final_outcome(string act) const;		//Returns the final outcome depending on your action.
};
		
		